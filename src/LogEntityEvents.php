<?php

namespace Drupal\log_entity;

/**
 * Document all events provided by the log_entity module.
 */
final class LogEntityEvents {

  /**
   * Log an event with the event reporter.
   *
   * @Event
   */
  const LOG_EVENT = 'log_event';

}
