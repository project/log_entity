<?php

namespace Drupal\log_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * The log entry event interface.
 */
interface LogEntityInterface extends ContentEntityInterface {

}
